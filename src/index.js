import "bootstrap/dist/css/bootstrap.css";
import { applyMiddleware, createStore } from "redux";
import { browserHistory, Router } from "react-router";
import promise from "redux-promise";
import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";
import reducers from "./reducers";
import routes from "./router";
import thunkMiddleware from "redux-thunk";

const createStoreWithMiddleware = applyMiddleware(promise, thunkMiddleware)(createStore);

ReactDOM.render(
  <Provider store={ createStoreWithMiddleware(reducers) }>
    <Router history={browserHistory} routes={ routes } />
  </Provider>, document.getElementById("root"));
