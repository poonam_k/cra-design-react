import "./app.css";
import React, { Component } from "react";
import Footer from "./components/Common/footer";
import Header from "./components/Common/header";
import sidenav from "./components/Common/sidenav";

class App extends Component {

  render() {
    return (
      <div>
        <Header />
        {/* <sidenav />*/}
        <div className="main-container" id="wrapper">
          {this.props.children}
        </div>

      </div>
    );
  }
}

export default App;
