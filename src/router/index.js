import { browserHistory, Route, Router } from "react-router";
import App from "../app";
import HomeComponent from "../components/Home/home";
import React from "react";
// Import components to route


const styles = {
  card: {
    border: "1px solid #eeeeee",
    borderRadius: "3px",
    padding: "15px",
    width: "250px",
  },
  image: {
    height: "200px",
    width: "250px",
  },
};

export default(
  <Router history={browserHistory}>

    <Route component={App}>
      <Route path="/" component={HomeComponent} styles={styles}></Route>

    </Route>
  </Router>
);
