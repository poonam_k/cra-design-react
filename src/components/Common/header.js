import "./css/common.css";
import "./css/responsive.css";
import React, { Component } from "react";
import { Button } from "reactstrap";
import { Link } from "react-router";
export default class Header extends Component {
  render() {
    return (
      <header>
        <div className="container-fluid">
          <div className="row align-items-center">
            <div className="col-sm">
              <div className="left-nav">
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">FAQ</a></li>
                  <li><a href="#">Blog</a></li>
                </ul>
              </div>
            </div>
            <div className="col-sm text-center">
              <h1 className="logo"><img alt="logo" src={require("./images/logo.jpg")} /></h1>

            </div>
            <div className="col-sm">
              <div className="right-nav">
                <ul className="pull-right">
                  <li><a href="#"><i className="fa fa-bell" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i className="fa fa-user-circle" aria-hidden="true"></i></a></li>
                  <li><a href="#"><span>Priel</span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
