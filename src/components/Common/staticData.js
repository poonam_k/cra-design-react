/**
 * the default static data for home page
 */
const dummyDescription = `when an unknown printer took a galley of type and scrambled it to make a type specimen book.
It has survived not only five centuries, but also the leap into electronic typesetting, 
remaining essentially unchanged.`;
const dummyPrice = 15;
const dummyTitle = "FULL STACK DEVELOPMENT FULL STACK DEVELOPMENT FULL STACK DEVELOPMENT";
const dummyTags = ["#education", "#computer", "#courses"];
const dummyDate = "July 24, 2017";
const icons_desktop = ["facebook", "googleplus", "twitter", "mail", "linkedin"];
const icons_mobile = ["messenger", "whatsapp", "twitter", "mail", "linkedin"];
const data = [];
for (let value in [1, 2, 3, 4, 5, 6, 7, 8]) data.push({ id: value, fav: false, title: dummyTitle, price: dummyPrice, description: dummyDescription, tags: dummyTags, date: dummyDate, icons_desktop, icons_mobile });

export default data;
