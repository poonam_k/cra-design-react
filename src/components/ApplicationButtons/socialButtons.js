import { Power2, TweenMax } from "gsap";
import React, { Component } from "react";
import allData from "../Common/staticData";
import CopyToClipboard from "react-copy-to-clipboard";
import Dimensions from "react-dimensions";
import MoreSharingButtons from "./moreSharingButtons";
import Transition from "react-transition-group";
import TransitionGroup from "react-addons-transition-group";

export default class Button extends Component {

  constructor() {
    super();
    this.state = {
      showMore: false,
      copied: false,
      height: null,
    };
    let icons = "";
    console.log(this.state);
    this.showToggle = this.showToggle.bind(this);
  }


  showToggle() {
    this.setState({ showMore: !this.state.showMore });

  }
  componentWillEnter(callback) {

    const { customId } = this.props;
    const el1 = document.getElementById("a1" + customId);
    const el2 = document.getElementById("a2" + customId);
    const el3 = document.getElementById("a3" + customId);
    let el4 = document.getElementById("a4" + customId);
    el1.style.opacity = el2.style.opacity = el3.style.opacity = el4.style.opacity = 0;
    TweenMax.fromTo(el1, 0.3, { x: -100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
    setTimeout(function() {
      TweenMax.fromTo(el2, 0.3, { x: -100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
    }, 200);
    setTimeout(function() {
      TweenMax.fromTo(el3, 0.3, { x: -100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
    }, 400);
    setTimeout(function() {
      TweenMax.fromTo(el4, 0.3, { x: -100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
    }, 600);

  }

  /**
   * @desc calling reverting animations
   * @param {*} callback 
   */
  componentWillLeave(callback) {

    const { customId } = this.props;

    const el = this.container;

    const el1 = document.getElementById("a1" + customId);
    const el2 = document.getElementById("a2" + customId);
    const el3 = document.getElementById("a3" + customId);
    let el4 = document.getElementById("a4" + customId);

    TweenMax.fromTo(el1, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback });
    setTimeout(function() {
      TweenMax.fromTo(el2, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback });
    }, 600);
    setTimeout(function() {
      TweenMax.fromTo(el3, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback });
    }, 400);
    setTimeout(function() {
      TweenMax.fromTo(el4, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback });
    }, 200);
    // TweenMax.fromTo(el, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback } );
  }
  checkmoreclicked(props) {
    const { customId, onExpand, expanded } = this.props;
    if (expanded) {
      return (
        <a href="javascript:void(0)" id={`more1-${customId}`}><span><img alt="google" src={require("../Common/images/googleplus.png")} className="log-img" /><span className="social-txt">Google +</span></span></a>
      );
    } else {
      return (
        <a href="javascript:void(0)" id={"a4" + customId}><span><img alt="more" src={require("../Common/images/more.png")} className="log-img" onClick={ onExpand }/><span className="social-txt"> More </span></span></a>
      );
    }
  }
  checkwindowsize(props) {
    const { data } = this.props;
    if (window.innerWidth > 767) {
      this.icons = data.icons_desktop;
    } else {
      this.icons = data.icons_mobile;
    }
  }
  showmorelinks(props) {
    const { customId, onExpand, expanded, data } = this.props;
    let icons_length = this.icons.length;
    if (expanded) {
      return this.icons.map((icon, key) => {
        if (key === 4) {
          return false;
        } else if (key < 4) {
          let tmpKey = key + 1;
          tmpKey = "a" + tmpKey + customId;
          return <a href="javascript:void(0)" id={tmpKey} ><span><img alt={icon} src={require(`../Common/images/${icon}.png`)} className="log-img" /><span className="social-txt">{icon}</span></span></a>;
        }
      });
    } else {
      return this.icons.map((icon, key) => {
        if (key === 3 && icons_length > 4) {
          let tmpKey = "a4" + customId;
          return <a href="javascript:void(0)" id={tmpKey}><span><img alt="more" src={require("../Common/images/more.png")} className="log-img" onClick={ onExpand }/><span className="social-txt"> More </span></span></a>;
        } else if (key < 3) {
          let tmpKey = key + 1;
          tmpKey = "a" + tmpKey + customId;
          return <a href="javascript:void(0)" id={tmpKey} ><span><img alt={icon} src={require(`../Common/images/${icon}.png`)} className="log-img" /><span className="social-txt">{icon}</span></span></a>;
        }
      });

    }
  }


  render() {
    const { customId, onExpand, expanded, data } = this.props;
    return (
      <div id={ customId }>
        <TransitionGroup>
          <div className="testDiv" id="testDiv" ref={(c) => this.container = c}>
            <div className="social-dv">
              {console.log("value is ", ((window.innerHeight > 420) && (window.innerWidth > 500)))}
              { this.checkwindowsize(this.props)}
              { (this.icons.length < 5) && <div>
                {this.icons.map((icon, key) => {
                  let tmpKey = key + 1;
                  tmpKey = "a" + tmpKey + customId;
                  return <a href="javascript:void(0)" id={tmpKey} ref={tmpKey} ><span><img alt={icon} src={require(`../Common/images/${icon}.png`)} className="log-img" /><span className="social-txt">{icon}</span></span></a>;
                })}
              </div>}
              { !(data.icons_desktop.length < 5) && <div>
                {this.showmorelinks(this.props)}
              </div>
              }

            </div>
          </div>
        </TransitionGroup>
        <TransitionGroup>
          {(this.state.showMore && this.props.toggle) ? <MoreSharingButtons customId={customId}/> : null}
        </TransitionGroup>
        <div className='testDiv'>
        </div>
      </div>

    );
  }
}

