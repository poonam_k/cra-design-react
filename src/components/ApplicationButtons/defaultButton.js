import React, { Component } from "react";
import { TweenMax } from "gsap";
export default class TestDiv extends Component {
  componentWillEnter(callback) {
    const el = this.container;
    // TweenMax.fromTo(el, 0.3, {y: 100, opacity: 0}, {y: 0, opacity: 1, onComplete: callback});
    TweenMax.fromTo(el, 0.3, { x: 100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
  }

  componentWillLeave(callback) {
    const el = this.container;
    TweenMax.fromTo(el, 0.3, { x: 0, opacity: 1 }, { x: 100, opacity: 0, onComplete: callback });

  }
  render() {
    const { data = {} } = this.props;
    return (
      <div className="transitin-dv">
        <div className="testDiv" ref={(c) => this.container = c}>
          <p className="p_second" id={`tags${data.id}`}>{data.tags.map((item) => <a>{item}&nbsp;</a>)}</p>

          <button type="button" className="btn" onClick={this.props.toggle}>RECOMMEND<i className="fa fa-caret-right" aria-hidden="true"></i>
          </button>
        </div>
        <p className="published-dv">Published: Jul 24,2017</p>
      </div>
    );
  }
}
