import React, { Component } from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import TransitionGroup from "react-addons-transition-group";
import { TweenMax } from "gsap";

export default class More extends Component {
  constructor() {
    super();
    this.state = {
      copied: false,
    };
    let icons = "";
  }
  componentWillEnter(callback) {
    // const el = this.container; 

    const { customId } = this.props;
    console.log("propssssss", this.props);
    let el4 = document.getElementById(`more4${customId}`);
    let el5 = document.getElementById(`more5${customId}`);
    let socialDiv = document.getElementById("testDiv");
    el4.style.opacity = el5.style.opacity = 0;
    TweenMax.fromTo(el4, 0.3, { x: -100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
    setTimeout(function() {
      TweenMax.fromTo(el5, 0.3, { x: -100, opacity: 0 }, { x: 0, opacity: 1, onComplete: callback });
    }, 200);

  }

  componentWillLeave(callback) {
    // alert ('leave')
    // const el = this.container;
    const { customId } = this.props;

    let el4 = document.getElementById(`more4${customId}`);
    let el5 = document.getElementById(`more5${customId}`);
    TweenMax.fromTo(el4, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback });
    setTimeout(function() {
      TweenMax.fromTo(el5, 0.3, { x: 0, opacity: 1 }, { x: -100, opacity: 0, onComplete: callback });
    }, 200);

  }
  checkwindowsize(props) {
    const { data } = this.props;
    if (window.innerWidth > 767) {
      this.icons = data.icons_desktop;
    } else {
      this.icons = data.icons_mobile;
    }
  }
  showlinks(props) {
    const { onExpand, customId } = this.props;
    let tmpArr = [];
    this.icons.map((icon, key) => {
      if (key > 3) {
        let tmpKey = key;
        tmpKey = "more" + tmpKey + customId;
        console.log("tmpkey", tmpKey);
        tmpArr.push(<a href="javascript:void(0)" id={tmpKey} ><span><img alt={icon} src={require(`../Common/images/${icon}.png`)} className="log-img" /><span className="social-txt">{icon}</span></span></a>
        );
      }


    });
    tmpArr.push(<a href="javascript:void(0)" id={`more5${customId}`} ><span><img alt='less' src={require("../Common/images/less.png")} className="log-img" onClick={ onExpand } /><span className="social-txt">less</span></span></a>);
    console.log("temparr ", tmpArr);
    return tmpArr;
  }
  render() {
    const { onExpand, customId } = this.props;
    return (
      <section id= {customId}>
        { this.checkwindowsize(this.props) }
        <TransitionGroup>
          <section className='testDiv moveup'>
            {this.showlinks(this.props)}
          </section>
        </TransitionGroup>
      </section>
    );
  }
}
