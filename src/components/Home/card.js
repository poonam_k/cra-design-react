import { DefaultButton, MoreSharingButtons, SocialButtons } from "../../components/ApplicationButtons/";
import CopyToClipboard from "react-copy-to-clipboard";
import PropTypes from "prop-types";
import React from "react";
import ReactCardFlip from "react-card-flip";
import TransitionGroup from "react-addons-transition-group";
import { TweenMax } from "gsap";

const wrappingWidth = 140;

/**
 * @desc the default react component to represent the card component
 */
class CardComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      flipped: this.props.isFlipped,
      showShare: this.props.showShareButtons,
      expandShare: this.props.expandShare,
    };

    this.toggleFlip = this.toggleFlip.bind(this);
    this.toggleShareButtons = this.toggleShareButtons.bind(this);
    this.toggleExpandShare = this.toggleExpandShare.bind(this);
  }

  /**
* toggle flipping content
*/
  toggleFlip() {
    this.setState({ flipped: !this.state.flipped });
  }
  /**
* toggle share buttons
*/
  toggleShareButtons() {
    this.setState({ showShare: !this.state.showShare });
  }

  toggleExpandShare() {
    this.setState({ expandShare: !this.state.expandShare });
  }
  setLike(fav, id) {
    var dataIndex = parseInt(id.substring(4, 5));
    if (fav) {
      this.props.disLike(dataIndex);
    } else {
      this.props.like(dataIndex);
    }
  }

  componentDidUpdate() {
    const { id } = this.props.data;
    const slidingElement = document.getElementById(`box${id}`);
    // const tagsElement = document.getElementById(`tags${id}`);
    const { expandShare, showShare } = this.state;
    const showExpand = this.state.expandShare;
    const { data } = this.props;

    if (expandShare) {
      console.log("hjello");
      // if (document.getElementById(`tags${id}`))
      // document.getElementById(`card-content${id}`).removeChild(document.getElementById(`tags${id}`));
      // // TweenMax.fromTo (tagsElement, 0.2, {opacity: 1}, {opacity: 0});
      // TweenMax.fromTo(slidingElement, 0.2, { y: 0 }, { y: -34 });
    } else {
      // TweenMax.fromTo (tagsElement, 0.2, {display: 'none', opacity: 0}, {display: 'block', opacity: 1});
      if (showShare && !document.getElementById(`tags${id}`)) {
        const element = document.getElementById(`card-content${id}`);
        let appendElement = document.createElement("p");
        appendElement.setAttribute("id", `tags${id}`);
        appendElement.setAttribute("class", "p_second");
        data.tags.map((item) => {
          const anchor = document.createElement("a");
          anchor.innerHTML = item + "&nbsp;";
          appendElement.innerHTML += item + "&nbsp;";

          appendElement.innerHTML += anchor;
        });
        element.appendChild(appendElement);
        TweenMax.fromTo(slidingElement, 0.2, { y: -34 }, { y: 0 });
        // document.getElementById ('card-content').appendChild (<p className="p_second" id={'tags'+ id}>{ data.tags.map (item => <a>{item}&nbsp;</a>)}</p>);
      }
      // document.getElementById ('card-content').appendChild (<p className="p_second" id={'tags'+ id}>{ data.tags.map (item => <a>{item}&nbsp;</a>)}</p>);
      // TweenMax.fromTo (slidingElement, 0.2, { y: -20 }, { y: 0 });
    }
  }

  /**
* @desc returns the trimmed form of the description string
* @param {*} inputString 
*/
  trimmedString(inputString) {
    if (inputString.length > wrappingWidth) {
      let trimmedString = inputString.substr(0, wrappingWidth);
      trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")));
      return (<label>{trimmedString} ... <a href="javascript:void(0)" onClick={() => this.setState({ flipped: !this.state.flipped })} > Read More </a></label>);
    }
    return inputString;
  }

  render(props) {
    const { flipped = false, showShare = false, expandShare = false } = this.state;
    const { data = {}, onLike = () => { }, id, showFav = false } = this.props;
    return (
      <ReactCardFlip isFlipped={flipped}>
        <section key='front' id={id} style={{ display: (showFav ? data.fav ? "block" : "none" : "block"), float: showFav ? data.fav ? "left" : "" : "left" }}>
          <section className='card' >
            <section className='card-image'>
              <img alt="logo" src={require("../Common/images/img_1.jpg")} className="log-img" />
              <span className="price-strip">{data.price}$ Per Lead</span>
              <section className="heart-img  " onClick={(e) => this.setLike(this.props.fav, this.props.id)} >
                <a href="javascript:void(0)" className={this.props.fav ? "active-like" : ""}></a>
              </section>
            </section>
            <section className='card-content' id={`card-content${data.id}`}>
              <div className="top-heading">
                <h2>
                  {data.title.length < 60 ? data.title : data.title.substr(0, 56) + " ..." }
                </h2>
              </div>
              <p className="p_first">
                {this.trimmedString(data.description)}
              </p>
            </section>
            <section className='recommend-dv recommend-frst'>

              <div className="wraperdiv">
                <TransitionGroup>
                  {!expandShare && showShare && <p className="p_second" id={`tags${data.id}`}>{data.tags.map((item) => <a>{item}&nbsp;</a>)}</p>}
                  {!showShare && <DefaultButton data={data} toggle={this.toggleShareButtons} />}
                  {showShare && <SocialButtons data={data} toggle={this.toggleShareButtons} onExpand={this.toggleExpandShare} expanded={expandShare} customId={"box" + data.id} />}
                </TransitionGroup>
                <TransitionGroup>
                  {expandShare && <MoreSharingButtons data={data} toggle={this.toggleShareButtons} onExpand={this.toggleExpandShare} expanded={expandShare} customId={`share${data.id}`} />}
                </TransitionGroup>
              </div>
              <TransitionGroup style={{ display: showShare ? "" : "none" }}>
                <p className='copy-link'><CopyToClipboard text="copy to clipboard success"
                  onCopy={() => this.setState({ copied: true })}>
                  <a href="javascript:void(0)" className="clip-link">Copy link</a>
                </CopyToClipboard></p>
              </TransitionGroup>
            </section>
          </section>
        </section>
        <section key="back" >
          <section className="card" style={{ height: this.props.cardHeight }}>
            <section className="card-content card-con-back">
              <button onClick={this.toggleFlip} className="front-btn"><img alt="logo" src={require("../Common/images/arrow.png")} /></button>
              <h2><img alt="logo" src={require("../Common/images/strip.jpg")} className="strip-img" /></h2>
              <section className="content-flow">
                <p className="p_first p_back_sec"> {data.description}</p>
                <p className="p_first">{data.description}</p>
                <p className="p_second">{data.tags.map((item) => <a>{item}&nbsp;</a>)}</p>
              </section>
            </section>
            <section className="recommend-dv">
              <p className="published-dv">Published: {data.date}</p>
            </section>
          </section>
        </section>
      </ReactCardFlip>
    );
  }

}

CardComponent.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    price: PropTypes.number,
    description: PropTypes.string,
    tags: PropTypes.arrayOf(PropTypes.string),
    date: PropTypes.string,
  }),
  isFlipped: PropTypes.bool,
  onLike: PropTypes.func,
  showShareButtons: PropTypes.bool,
};

export default CardComponent;
