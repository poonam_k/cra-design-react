import { DefaultButton, SocialButtons } from "../../components/ApplicationButtons/";
import React, { Component } from "react";
import allData from "../Common/staticData";
import CardComponent from "./card";
import PropTypes from "prop-types";
import ReactCardFlip from "react-card-flip";
import TransitionGroup from "react-addons-transition-group";

var _ = require("lodash");
// let likeData = [];


const styles = {};

class AppFirstPage extends Component {
  constructor(props) {
    super(props);
    const pairData = [];
    let indexer = 0;
    this.state = { data: allData, likeData: [] };
    this.resizeEvent = this.resizeEvent.bind(this);
  }

  likeButtonClick(event) {
    console.log("value of like ", this.state.likeData);
    let element = event.target;
    if (element.classList.contains("active-like")) {
      this.setState({ showFav: false });
      // likeData = [];
      // this.setState({ data: this.checkfav });
      element.classList.remove("active-like");
      this.setState({ tempObject: undefined });
    } else {
      console.log(this.state);
      this.setState({ showFav: true });
      // this.setState({ data: this.state.likeData });
      element.classList.add("active-like");
      const tempObject = [];
      this.state.data.map((item) => {
        if (item.fav) tempObject.push(item);
      });
      this.setState({ tempObject: tempObject });

    }

  }

  componentDidMount() {
    let tis = this;
    setTimeout(function() {
      tis.resizeEvent();
    }, 1000);

    window.addEventListener("resize", this.resizeEvent);
  }

  resizeEvent() {
    const stateObject = {};
    this.state.data.map((item) => {
      stateObject[`cardHeight${item.id}`] = document.getElementById(`card${item.id}`).clientHeight;
    });

    this.setState(stateObject);
  }

  // checkelement(element,index){

  //   this.checkfav();
  //   console.log("inside checkelement ",element)
  //   return element.map((value)=>
  //   {
  //     console.log("given value",value);
  //     if (value.id == index){
  //       return value
  //     }
  //   }
  //   )

  // }

  like(index) {
    // console.log("state.data[index]=====",this.state.data[index] );
    console.log("state.likeData=====", _.find(this.state.likeData, function(chr) {
      return chr.id == index;
    }));

    // console.log("likedata ===",_.findKey(users, function(o) { return o.age < 40; }))

    if (this.state.data[index]) {

      let nData = JSON.parse(JSON.stringify(this.state.data.slice()));
      console.log(JSON.parse(JSON.stringify(this.state.data.slice())));
      nData[index].fav = true;
      console.log(nData[index]);
      this.setState({ data: nData });
      this.state.likeData.push(nData[index]);
    }

  }

  disLike(index) {
    if (this.state.data[index]) {
      let nData = JSON.parse(JSON.stringify(this.state.data.slice()));
      nData[index].fav = false;
      this.setState({ data: nData });
      this.state.likeData.splice(index, 1);
    }
  }

  checkfav() {
    let nData = JSON.parse(JSON.stringify(this.state.data.slice()));
    this.state.likeData.map((obj) =>
      nData[obj.id].fav = true
    );
    // this.setState({ data: nData })
    return nData;

  }

  liRender(item, index, masterIndex) {
    ;
    console.log("final data is :: ", this.state.data);
    if (index >= masterIndex && index < masterIndex + 4) {
      return (
        this.state.showFav ? item.fav ?
          <li className="col-sm-6 col-md-6 col-lg-3" key={item.id} style={{ height: `${this.state[`cardHeight${item.id}`]}px` }}>
            <CardComponent showFav={this.state.showFav} cardHeight={`${this.state[`cardHeight${item.id}`]}px`} data={item} fav={item.fav} id={`card${item.id}`} like={this.like.bind(this)} disLike={this.disLike.bind(this)} onLike={(e) => this.likeButtonClick(e, item.id)} />
          </li> : null : <li className="col-sm-6 col-md-6 col-lg-3" key={item.id} style={{ height: `${this.state[`cardHeight${item.id}`]}px` }}>
          <CardComponent showFav={this.state.showFav} data={item} fav={item.fav} cardHeight={`${this.state[`cardHeight${item.id}`]}px`} id={`card${item.id}`} like={this.like.bind(this)} disLike={this.disLike.bind(this)} onLike={(e) => this.likeButtonClick(e, item.id)} />
        </li>
      );
    }
  }

  ulRender(item, index, data) {
    if (index % 4 === 0) {
      let masterIndex = index;
      return (
        <ul key={index} className="card-content-outer">
          {
            data.map((item, index, arr) => (
              this.liRender(item, index, masterIndex)
            ))
          }
        </ul>
      );
    }
  }

  render() {
    return (
      <div className="home-container">
        <div className="heart-dv">
          <div className="container">
            <div className="row">
              <div className="offer-dv">Show favourite offers only <div className="heart-img" onClick={this.likeButtonClick.bind(this)}>
                <a href="javascript:void(0)"></a>
              </div></div>
            </div>
          </div>
        </div>
        <div className="container">
          <section className="row">
            {
              this.state.tempObject && this.state.tempObject.length > 0 ?
                this.state.tempObject.map((item, index) => (
                  this.ulRender(item, index, this.state.tempObject)
                )) :
                this.state.data.map((item, index) => (
                  this.ulRender(item, index, this.state.data)
                ))
            }
          </section>
        </div>
      </div>
    );
  }
}

AppFirstPage.propTypes = {
  styles: PropTypes.object,
};

export default AppFirstPage;
