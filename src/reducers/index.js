/**
 * @reducer       : index reducer
 * @description   :
 * @Created by    : smartData
 */

import { combineReducers } from "redux";
// import LoginReducer from './LoginReducer/LoginReducer';

import { reducer as formReducer } from "redux-form"; // SAYING use redux form reducer as reducer

const rootReducer = combineReducers({
  form: formReducer,
});

export default rootReducer;
